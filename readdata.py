# -*- coding: utf-8 -*-

"""
Functions for reading data.
"""

import unicodecsv as csv
import numpy as np
import logging
from collections import defaultdict
from StringIO import StringIO

import utils
from StringIO import StringIO

def load_text_embeddings(filename):
    """
    Load any embedding model written as binary numpy text

    :param filename: path to embeddings file
    :return: a tuple (defaultdict, array). The dict
        maps words to indices. Index 0 is for rare word.
    """
    words = defaultdict(int)

    def read_embeddings(filename, starting_index):
        """
        Auxiliary function for the actual file reading
        """
        vectors = []
        index = starting_index
        with open(filename, 'rb') as f:
            for i, line in enumerate(f):
                try:
                    line = unicode(line, 'utf-8')
                except UnicodeDecodeError:
                    print('Decoding error in line %d (skipped)' % i)
                    continue

                line = line.strip()
                if line == '':
                    continue

                fields = line.split()
                if len(fields) != 301:
                    print('Expected 301 fields (word + 300 values), but found %d '
                          'at line %d' % (len(line), i))
                    continue

                word = fields[0]
                words[word] = index
                index += 1
                vector = np.array([float(x) for x in fields[1:]])
                vectors.append(vector)

        return vectors

    index = 1
    model = read_embeddings(filename, index)
    vector_size = len(model[0])
    model.insert(0, np.random.uniform(-0.1, 0.1, vector_size))

    model = np.array(model)

    return words, model


def read_gold_file(path):
    """
    Read a file with the gold answers and return a dictionary mapping
    query ids to FAQ ids.
    """
    gold_answers = {}
    with open(path, 'rb') as f:
        for line in f:
            qid, faqid = line.split()
            gold_answers[qid] = faqid

    return gold_answers


def read_system_output(path):
    """
    Read a file in the format requested by the task organizers.
    Return a dictionary mapping query ids to AnsweredQuery
    objects.
    """
    answered_queries = {}
    with open(path, 'rb') as f:
        for line in f:
            qid, faqid, score = line.split()
            score = float(score)
            if qid in answered_queries:
                answered_queries[qid].add_answer(faqid, score)
            else:
                q = utils.AnsweredQuery(qid)
                q.add_answer(faqid, score)
                answered_queries[qid] = q

    return answered_queries


def load_vocabulary(path):
    """
    Loading a file with the vocabulary corresponding to an embedding model.

    :param path: path to the file (should be UTF-8!)
    :return: a python dictionary mapping words to indices in the
        embedding matrix
    """
    logging.info('Reading vocabulary...')
    with open(path, 'rb') as f:
        text = unicode(f.read(), 'utf-8')

    words = text.splitlines()
    values = range(len(words))
    word_dict = dict(zip(words, values))

    return word_dict


def read_faq(path, replacements=None):
    """
    Read the FAQ csv file and return it as a list of tuples
    :param path: path to the file
    :param replacements: dictionary mapping some strings to their
        replacements
    :return: list of tuples
    """
    logging.info('Reading FAQ...')
    with open(path, 'rb') as f:
        text = f.read().decode('utf-8')

    if replacements is not None:
        for key in replacements:
            text = text.replace(key, replacements[key])

    # csv module has to work with bytestrings, so convert them back
    text = text.encode('utf-8')
    reader = csv.reader(StringIO(text), delimiter=';', quotechar='"')
    lines = [line for line in reader]

    if lines[0][0] == 'id':
        # remove header
        lines = lines[1:]

    return lines


def read_acronyms(path):
    """
    Read the file with acronyms (it should have one acronym and its full
    form by line, separated by whitespace or tab).

    Return a dictionary mapping the full form to the acronym.

    If path is None, return an empty dictionary.
    """
    acronyms = {}
    if path is None:
        return acronyms

    with open(path, 'rb') as f:
        text = f.read().decode('utf-8')

    lines = text.splitlines()
    for line in lines:
        line = line.lower().strip()
        if len(line) == 0:
            continue

        acronym, full = line.split('\t', 1)
        acronyms[full] = acronym

    return acronyms


def read_mwes(path):
    """
    Read the file with MWE's (it should be a list with one expression
    per line) and return a data structure with them. It is a dictionary
    in which the the first word in all MWEs are mapped to another
    dictionary containing all their possibile second words, and so forth,
    until the last word which is mapped to None.

    Example: a dictionary containing "sevizio idrico" and "servizio clienti"
    would be:
    {'servizio': {'clienti': None, 'idrico': None}}
    """
    mwes = {}
    if path is None:
        return mwes

    with open(path, 'rb') as f:
        text = f.read().decode('utf-8')

    lines = text.splitlines()
    for line in lines:
        mwe = line.lower().strip()
        parts = mwe.split()

        upper_level_dict = mwes
        for part in parts[:-1]:
            upper_level_dict[part] = {}
            upper_level_dict = upper_level_dict[part]
        upper_level_dict[parts[-1]] = None

    return mwes


def read_queries(filename, mwes=None, replacements=None):
    """
    Read the given file with input queries. Each query should be in the
    format "id <TAB> question"

    :param mwes: dictionary of MWE's
    :param replacements: dictionary of replacements
    :return: a tuple (ids, questions); ids is a numpy array and questions
        is a list of tokenized sentences
    """
    if replacements is None:
        replacements = {}

    ids = []
    questions = []
    with open(filename, 'rb') as f:
        for line in f:
            line = unicode(line.lower(), 'utf-8')
            id_, question = line.split('\t', 1)
            ids.append(int(id_))
            for key in replacements:
                question = question.replace(key, replacements[key])

            questions.append(utils.tokenize(question, mwes))

    ids = np.array(ids)
    return ids, questions


def write_queries(queries, path):
    """
    Write queries to a file. Used for debugging only.
    """
    lines = []
    for query in queries:
        tokens = []
        for token in query:
            if isinstance(token, tuple):
                tokens.append('_'.join(token))
            else:
                tokens.append(token)

        lines.append(tokens)

    with open(path, 'wb') as f:
        text = '\n'.join([' '.join(line) for line in lines])
        f.write(text.encode('utf-8'))
