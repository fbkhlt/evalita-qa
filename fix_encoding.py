"""
Function to read strings with text in mixed encodings
(cp1252 and UTF-8) and return everything in UTF-8.
"""

import codecs

last_position = -1


def mixed_decoder(unicode_error):
    """
    This internal function is used as a callback to
    str.decode
    """
    global last_position
    string = unicode_error[1]
    position = unicode_error.start
    if position <= last_position:
        position = last_position + 1
    last_position = position
    new_char = string[position].decode("cp1252")
    #new_char = u"_"
    return new_char, position + 1


def register_mixed_decoder():
    """
    This function register the decoder for mixed encodings
    in the codecs module.
    """
    codecs.register_error("mixed", mixed_decoder)
