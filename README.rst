EVALITA QA
==========

The best configuration on our dataset had the following parameters:

`-n 5` (number of words used to compute SDF)
`-b --max` (take the result with maximum confidence between FAQ questions and answers)
`--mwe data/mwes.txt`
`--acro data/acronyms.tsv`
