# -*- coding: utf-8 -*-

from __future__ import division, print_function

"""
Script to read the output of the system and compare
against the gold data.

System file should be formatted as
<query id> <faq id> <score>

Gold file should be
<query id> <faq id>
"""

import argparse
import numpy as np

import readdata


def evaluate_performance(answered_queries, gold_answers, faqs, queries, verbose):

    """
    Accuracy including empty answers is computed as:

    c@1 = (n_r + n_u * n_r / n) / n

    where:
        n: total number of questions
        n_r: number of questions correctly answered
        n_u: number of questions unanswered
    """
    n = len(gold_answers)
    n_a = len(answered_queries)
    n_u = n - n_a
    n_r = 0
    map_hits = 0
    top10_hits = 0
    answers_not_in_top10 = []

    answers_not_in_top10_gs = []
    answers_not_in_top10_answer = []

    confidences_right = []
    confidences_wrong = []

    for qid in gold_answers:
        gold_answer = gold_answers[qid]
        if qid not in answered_queries:
            continue

        answered_query = answered_queries[qid]
        system_answer, conf = answered_query.get_top_answer(return_confidence=True)
        top25 = answered_query.get_top_n(25)

        for i, faqid in enumerate(top25, 1):
            if gold_answer == faqid:
                map_hits += (1 / i)
                continue

        top10 = answered_query.get_top_n(10)
        if gold_answer in top10:
            top10_hits += 1
        else:
            answers_not_in_top10.append(qid)
            for element in faqs:
                if element[0] == gold_answer:
                    answers_not_in_top10_gs.append(element[1])
                if element[0] == system_answer:
                    answers_not_in_top10_answer.append(element[1])

        if gold_answer == system_answer:
            n_r += 1
            confidences_right.append(conf)
        else:
            confidences_wrong.append(conf)

    confidences_right = np.array(confidences_right)
    confidences_wrong = np.array(confidences_wrong)

    map_score = 100*map_hits / n
    c_at_1 = 100*(n_r + n_u * n_r / n) / n
    top10_acc = 100*top10_hits / n
    print('c@1: %.2f' % c_at_1)
    print('MAP: %.2f' % map_score)
    print('Top 10 accuracy (with oracle): %.2f' % top10_acc)

    mean = confidences_right.mean()
    std = confidences_right.std()
    print('Average and std of confidence when right: {:.2f} +- {:.2f}'.format(mean, std))
    mean = confidences_wrong.mean()
    std = confidences_wrong.std()
    print('Average and std of confidence when wrong: {:.2f} +- {:.2f}'.format(mean, std))

    if verbose:
        print('Queries without answer in the top 10 and answer id:')

        for qid, gs, sa in zip(answers_not_in_top10, answers_not_in_top10_gs, answers_not_in_top10_answer):
            print(qid, gold_answers[qid])
            for id, sentence in zip(queries[0], queries[1]):
                if id == int(qid):
                    print(' '.join(sentence))
            print(gs)
            print(sa)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-system', help='File produced by the system')
    parser.add_argument('-gold', help='File with gold labels')
    parser.add_argument('-faq', help='File with faq')
    parser.add_argument('-query', help='File with query')
    parser.add_argument('-v', help='Detailed output', action='store_true', dest='verbose')

    args = parser.parse_args()

    gold_answers = readdata.read_gold_file(args.gold)
    answered_queries = readdata.read_system_output(args.system)
    faqs = readdata.read_faq(args.faq)
    queries = readdata.read_queries(args.query)

    evaluate_performance(answered_queries, gold_answers, faqs, queries, verbose=args.verbose)