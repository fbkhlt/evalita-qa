# -*- coding: utf-8 -*-

from __future__ import division, print_function

"""
Script to compare the confidence scores obtained when looking at FAQs
against the one obtained with the answers
"""

import argparse

import readdata


def compare_answers(answered1, answered2, gold_answers, margin):
    """
    Compare some metrics about two sets of answered queries

    :param answered1: dictionary (qid -> AnsweredQuery)
    :param answered2: dictionary (qid -> AnsweredQuery)
    :param gold_answers: dictionary (qid -> faqid)
    """
    sys1_hits = 0
    sys2_hits = 0
    oracle_hits = 0
    bigger_confidence_hits = 0
    conf2_bigger = 0
    conf2_bigger_and_right = 0
    not_in_top10 = []

    for qid in gold_answers:
        gold_answer = gold_answers[qid]
        answered_query1 = answered1[qid]
        answered_query2 = answered2[qid]
        answer1, conf1 = answered_query1.get_top_answer(True)
        answer2, conf2 = answered_query2.get_top_answer(True)
        correct1 = False
        correct2 = False

        if (conf1 + margin) > conf2:
            if gold_answer == answer1:
                bigger_confidence_hits += 1
        else:
            conf2_bigger += 1
            if gold_answer == answer2:
                bigger_confidence_hits += 1
                conf2_bigger_and_right += 1

        if gold_answer == answer1:
            sys1_hits += 1
            correct1 = True

        if gold_answer == answer2:
            sys2_hits += 1
            correct2 = True
        if correct1 or correct2:
            oracle_hits += 1

        if not correct1 and not correct2:
            top10 = answered_query1.get_top_n(10)
            if gold_answer not in top10:
                top10 = answered_query2.get_top_n(10)
                if gold_answer not in top10:
                    not_in_top10.append(qid)

    print('System 1 hits: {}'.format(sys1_hits))
    print('System 2 hits: {}'.format(sys2_hits))
    print('Oracle hits: {}'.format(oracle_hits))
    print('Hits of the system with highest confidence: '
          '{}'.format(bigger_confidence_hits))
    print('System 2 had more confidence than '
          '(sys1 + {}) {} times'.format(margin, conf2_bigger))
    print('\tout of which, it was right {} times'.format(conf2_bigger_and_right))
    print('The following queries were not in the top 10 of any system: ' +
          ' '.join(not_in_top10))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('system1', help='File produced by the first system')
    parser.add_argument('system2', help='File produced by the second system')
    parser.add_argument('gold', help='File with gold labels')
    parser.add_argument('-m', help='Margin by how much system 1 should be preferred '
                                   '(used when picking the best from each one)',
                        dest='margin', default=0., type=float)
    args = parser.parse_args()

    gold_answers = readdata.read_gold_file(args.gold)
    answered1 = readdata.read_system_output(args.system1)
    answered2 = readdata.read_system_output(args.system2)

    compare_answers(answered1, answered2, gold_answers, args.margin)
