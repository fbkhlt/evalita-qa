# -*- coding: utf-8 -*-

"""
Utility functions and types.
"""

import json
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from collections import namedtuple


stopwords = set(stopwords.words('italian'))
stopwords.update(['.', ',', '?', '"', "'", "''", "``"])
# these words are in NLTK's stopword list but are meaningful in this context
stopwords.difference_update(['dove', 'come', 'com', 'dov', 'chi', 'si', u'perché'])

Answer = namedtuple('Answer', ['id', 'score'])


class AnsweredQuery(object):
    """
    Dummy class with some variables
    """
    def __init__(self, query_id):
        self.id = query_id
        self.answers = []

    def add_answer(self, faq_id, confidence):
        answer = Answer(faq_id, confidence)
        self.answers.append(answer)

    def get_top_answer(self, return_confidence=False):
        if return_confidence:
            return self.answers[0]

        return self.answers[0].id

    def get_top_n(self, n):
        return [a.id for a in self.answers[:n]]


def get_tokenized_question_and_answers(faq, mwes):

    """
    Read the FAQ and return tokenized questions combined
    (concatenated) with their answers
    :param faq: list of tuples
    :return: list of lists of tokens
    """
    texts = []
    for item in faq:
        question = tokenize(item[1], mwes)
        answer = tokenize(item[2], mwes)

        texts.append(question + answer)

    return texts


def get_tokenized_answers(faq, mwes):
    """
    Read the FAQ and return tokenized answers
    :param faq: list of tuples
    :param mwes: collection of MWEs
    :return: list of lists of tokens
    """
    return tokenize_sentences([item[2] for item in faq], mwes)


def get_tokenized_questions(faq, mwes):
    """
    Read the FAQ and return tokenized questions
    :param faq: list of tuples
    :param mwes: collection of MWEs
    :return: list of lists of tokens
    """
    return tokenize_sentences([item[1] for item in faq], mwes)


def get_tokenized_tags(faq, mwes=None):
    """
    Read the FAQ and return tokenized questions
    :param faq: list of tuples
    :return: list of lists of tokens
    """
    return tokenize_sentences([item[3] for item in faq], mwes)


def tokenize_sentences(sentences, mwes):
    """
    :param sentences: a list of strings (sentences)
    :param mwes: collection of MWEs
    :return: a list of lists of strings (tokens)
    """
    return [tokenize(sentence, mwes) for sentence in sentences]


def treat_mwes(tokens, mwes):
    """
    Internal helper function
    :param tokens: list of tokens
    :param mwes: dictionary of MWEs
    :return:
    """
    found_mwes = []
    for i, token in enumerate(tokens):
        partially_found_mwe = []

        # check if this token is the first in a MWE
        if token in mwes:
            # keep checking the following ones until either the whole MWE is found
            # or it is not complete
            next_pos = i + 1
            current_mwe_dict = mwes
            while True:
                current_mwe_dict = current_mwe_dict[token]
                if current_mwe_dict is None:
                    # this means the whole MWE was found
                    partially_found_mwe.append(token)
                    found_mwes.append(tuple(partially_found_mwe))
                    break

                if next_pos == len(tokens):
                    # after sentence end
                    break

                next_token = tokens[next_pos]
                if next_token not in current_mwe_dict:
                    break

                partially_found_mwe.append(token)
                token = next_token
                next_pos += 1

    tokens.extend(found_mwes)
    return tokens


def tokenize(text, mwes=None):
    """
    Pre-process and tokenize text. Lower case, remove apostrophes and
    stopwords.

    :param text: string
    :param mwes: collection of MWEs
    :return: list of string tokens. if `mwes` is not None, tuples
        with the MWEs will be appended to this list.
    """
    tokens = word_tokenize(text.lower().replace("'", ' '), 'italian')
    if mwes is not None:
        tokens = treat_mwes(tokens, mwes)
    return tokens
    # return [token for token in tokens if token not in stopwords]


def load_json_corpus(path):
    """
    Load a corpus in JSON format. The JSON file should contain a list
    of sentences, such that each sentence is a list of tokens.

    :param path: path to the json file
    :return: a list of lists of tokens
    """
    with open(path, 'rb') as f:
        corpus = json.load(f)
    return corpus
