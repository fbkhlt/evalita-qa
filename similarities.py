# -*- coding: utf-8 -*-

from __future__ import division, print_function

"""
Script to compute word embedding-based similarity between a question
and each answer in the FAQ.
"""

import argparse
import numpy as np
import logging
from collections import Counter

from scipy.linalg import norm
from scipy.spatial.distance import cdist

import utils
import readdata

np.seterr(all='raise')


def find_similar_words(word, word_dict, model, num_words=10):
    """
    Return the `num_words` most similar to the given one.
    """
    ind = word_dict[word]
    vec = model[ind]
    dot_product = model.dot(vec)
    sims = dot_product / norm(vec) / norm(model)
    inds = sims.argsort()[::-1][1:num_words+1]
    inv_wd = {v: k for k, v in word_dict.items()}
    return [inv_wd[ind] for ind in inds]


def compute_df_from_questions_or_corpus(questions, corpus_path=None, tags=None):
    """
    Compute the document frequency (DF) of words appearing in either
    the whole given corpus or just the questions, if corpus_path is None.
    :param questions: a list of lists of tokens
    :param corpus_path: a path to a json file
    :param mwes: set with MWEs; each one will have its DF computed
    :return: a dictionary mapping words to DF
    """
    if corpus_path is not None:
        logging.info('Computing DF from external corpus...')
        corpus = utils.load_json_corpus(corpus_path)
    else:
        logging.info('Computing DF from FAQ questions...')
        corpus = questions
    return compute_df(corpus, tags)


def compute_df(documents, tags=None):
    """
    Compute the document frequency (DF) of all words in the documents.
    It is computed as the number of documents having the word divided by
    the total number of documents.

    :param documents: a list of lists of tokens
    :param mwes: set with MWEs; each one will have its DF computed
    :return: a dictionary mapping words to DF
    """
    counter = Counter()

    if tags is not None:
        for doc, tag in zip(documents,tags):
            wordset = set(doc)
            tagset = set(tag)
            wordset.difference_update(tagset)
            counter.update(wordset)
    else:
        for doc in documents:
            wordset = set(doc)
            counter.update(wordset)

    # normalize by number of documents
    for word in counter:
        counter[word] /= len(documents)

    return counter


def compute_cosines_memory_friendly(smaller, bigger):
    """
    Compute the cosines between the 2-d arrays smaller and bigger.
    It splits the bigger array in two if it doesn't fit in memory.

    :param smaller: 2d array
    :param bigger: 2d array
    :return: 2d array
    """
    try:
        dists = cdist(smaller, bigger, 'cosine')
    except MemoryError:
        half_bigger = len(bigger) // 2
        dists1 = cdist(smaller, bigger[:half_bigger])
        dists2 = cdist(smaller, bigger[half_bigger:])
        dists = np.hstack(dists1, dists2)

    # cdist returns distances, which are 1 - actual cosine
    cosines = 1 - dists
    return cosines


def compute_sdf(df, embeddings, word_dict, target_words, top_n):
    """
    Compute the SDF (similarity document frequency) for all words in
    the list query_words.

    It is computed as the document frequency of a word plus those
    of other similar words, weighted by their cosine similarity.

    :param df: dictionary mapping words to DF (proportion of documents they
        appear in)
    :param embeddings: 2-d numpy array
    :param word_dict: dictionary mapping words to indices in the embedding
        model
    :param target_words: list of words we want to compute the SDF for
    :param top_n: how many words to include in DF computation
    :return: a dictionary mapping all words in `query_words` that have an
        embeddings to their similarity document frequency
    """
    logging.info('Computing SDF values...')
    # what this function does is:
    # 1) list all words that have a DF value and take their embeddings
    # 2) compute the cosine between words found in 1) and target_words
    # 3) compute the SDF value for all words in target_words

    # (only words in word_dict have an embedding)
    df_words = [word for word in df.keys() if word in word_dict]
    target_words = [word for word in target_words if word in word_dict]

    df_words_indices = [word_dict[word] for word in df_words]
    target_word_indices = [word_dict[word] for word in target_words]

    # take the vectors of the target words we want to create SDF for
    # and, separately, those of the ones with DF values
    df_vectors = embeddings[df_words_indices]
    target_vectors = embeddings[target_word_indices]
    cosines = compute_cosines_memory_friendly(target_vectors, df_vectors)

    # plus 1 for the word itself
    top_n += 1
    most_similar_inds = cosines.argsort(1)[:, -top_n:]
    num_words = len(target_vectors)
    rows = np.tile(np.arange(num_words), top_n).reshape((top_n, num_words)).T
    similar_cosines = cosines[rows, most_similar_inds]

    sdf = {}
    for i, word in enumerate(target_words):
        similar_words = [df_words[ind] for ind in most_similar_inds[i]]
        similar_df = np.array([df[sim_word] for sim_word in similar_words])
        # the word itself is already included in the most similar results
        sdf[word] = np.sum(similar_df * similar_cosines[i])

    return sdf


def create_new_embeddings(wd, model, new_words):
    """
    Create new random embeddings for words that don't already have one.

    :param wd: dictionary of words to indices
    :param model: 2-d numpy array
    :param new_words: set of words (they may or not be in the model)
    :return: a tuple (wd, model)
    """
    vector_dim = model.shape[1]
    num_new_words = 0
    new_vectors = []

    for word in new_words:
        if word not in wd:
            ind = len(wd)
            wd[word] = ind
            new_vector = np.random.normal(0, 0.1, vector_dim)
            new_vectors.append(new_vector)
            num_new_words += 1

    logging.info('Created random embeddings for %d new word(s)' % num_new_words)
    model = np.vstack((model, new_vectors))

    return wd, model


def generate_text_embedding(tokens, word_dict, model, df=None):
    """
    Generate a vector representation for a piece of text.

    :param tokens: list of pre-processed tokens
    :param word_dict: mapping of word to indices
    :param model: numpy array (vocabulary, embedding size)
    :param df: dictionary mapping words to document frequency or SDF
    :return: numpy array
    """
    valid_tokens = [token for token in tokens if token in word_dict and
                    token not in utils.stopwords]

    inds = [word_dict[token] for token in valid_tokens]
    vectors = [model[ind] for ind in inds]
    array = np.array(vectors)

    if df is not None:
        coefficients = np.array([df[token] for token in valid_tokens],
                                dtype=np.float)
        coefficients[coefficients == 0] += 0.1
        array = (array.T * np.log(1/coefficients)).T

    return array.mean(0)


def compute_distances(query_matrix, faq_matrix):
    """
    Generate the distance score of answering each query with each
    possible FAQ item. The confidence can be found as (1 - distance).
    We leave them as distances instead of confidence to make some
    other computations easier.

    It computes the cosine distance between each query-FAQ combination.
    If `num_common_oov` is given, some transformations are performed
    in order to proportionally increase similarity.

    :param query_matrix: 2-d numpy array with question embeddings
        (each row represents a query)
    :param faq_matrix: -d numpy array with FAQ embeddings
        (each row represents a question/answer or something in the FAQ)
    :return: a 2-d array (queries, faqs) with the distance scores
    """
    logging.info('Ranking FAQs...')

    # matrix with distance between each question/answer combination
    return cdist(query_matrix, faq_matrix, 'cosine')


def eval_accuracy(distances, correct_answer):
    """
    Evaluate the accuracy with which questions and answers are matched.

    :param distances: 2-d array (queries, faqs) with distances (1 - confidence)
    :param correct_answer: a 1-d numpy array containing the number of
        the correct answer for each question. It should be 0-based
    :return: tuple of Python floats (accuracy, MAP score)
    """
    logging.info('Computing accuracy...')

    # argmin has the shortest distance
    argmins = distances.argmin(1)
    num_queries = len(argmins)
    hits = np.sum(argmins == correct_answer)
    acc = 100 * hits / num_queries

    # get the 25 closest answers to each question
    map_score = 0
    inds = distances.argsort(1)[:, :25]
    # compute the MAP and also how many correct answers in the top 10
    hits_top10 = 0
    for i in range(25):
        # i-th position
        hits = inds[:, i] == correct_answer
        num_hits = np.sum(hits)
        map_score += num_hits / (i + 1)
        if i < 10:
            hits_top10 += num_hits

    map_score /= num_queries
    top10_percent = 100 * hits_top10 / num_queries
    print('{}% of the answers can be found in the top 10'.format(top10_percent))

    return acc, map_score


def generate_embedding_matrix(texts, wd, model, df=None):
    """
    Generate an embedding matrix combining the embeddings for
    each text in texts.

    :param texts: list of lists of tokens
    :param wd: word dictionary
    :param model: 2-d numpy array with embeddings
    :param df: dictionary mapping words to document frequency or SDF
    :return: 2-d numpy array
    """
    emb_list = [generate_text_embedding(tokens, wd, model, df)
                for tokens in texts]
    return np.array(emb_list)


def get_paraphrase_answer_index(paraphrases):
    """
    Return the index of the correct answer to each paraphrase.
    The index is just the same position of the paraphrase; and this function
    properly treats empty paraphrases.

    :param paraphrases: a list of lists
    :return: a 1-d numpy array
    """
    return np.array([i for i in range(len(paraphrases))
                     if len(paraphrases[i]) > 0])


def write_answers(filename, distances, query_ids, faq_ids, threshold=0):
    """
    Write the system answer to a file.

    :param filename: path to file to be written
    :param distances: 2-d array (queries, faqs)
    :param query_ids: array with the id's of the queries, such that
        query_ids[n] has the id of the n-th row in confidences
    :param faq_ids: analogous to query_ids; relates columns to ids
    :param threshold: minimum confidence value an FAQ must have
        in order to be included in the output.
    """
    top_indices = distances.argsort(1)[:, :25]
    confidences = 1 - distances
    written = 0

    with open(filename, 'wb') as f:
        for i, row in enumerate(distances):
            best_confidence = confidences[i, top_indices[i, 0]]

            # if the best confidence for this question is below the threshold,
            # ignore it
            if best_confidence < threshold:
                continue

            written += 1
            query_id = query_ids[i]
            for j in top_indices[i]:
                faq_id = faq_ids[j]
                confidence = confidences[i, j]
                line = '%d\t%d\t%f\n' % (query_id, faq_id, confidence)
                f.write(line)

    logging.info('Written %d answers' % written)


def generate_mwe_embeddings(df, wd, embeddings):
    """
    Generate embeddings for the MWEs combining the embeddings of their
    words, weighted by their DF. MWEs are found as tuples in the df.

    :param df: dictionary mapping words and MWE tuples to DF
    :param wd: dictionary mapping words to indices
    :param embeddings: 2-d numpy array
    :return: a tuple (wd, embeddings) with the new MWEs. The new wd
        is a copy of the old one
    """
    new_wd = wd.copy()
    mwes = [item for item in df if isinstance(item, tuple)]
    new_vectors = []

    logging.info('Generating embeddings for %d MWEs...' % len(mwes))

    for mwe in mwes:
        # this will break if there are OOV words in the MWE
        # and they weren't added to the embeddings model
        part_inds = [wd[part] for part in mwe]
        vectors = embeddings[part_inds]
        weights = [df[part] for part in mwe]
        weighted_vectors = (vectors.T * weights).T
        new_vector = weighted_vectors.mean(0)
        new_vectors.append(new_vector)

        ind = len(new_wd)
        new_wd[mwe] = ind

    embeddings = np.vstack((embeddings, new_vectors))
    return new_wd, embeddings


def get_word_set(sentences):
    """
    Return the set of words occurring in the given sentences
    :param sentences: a list of lists of tokens
    :return: a set
    """
    return set(token for sent in sentences for token in sent)


def combine_distances(distances, weight, take_max):
    """
    Combine linearly two distance matrices.
    """
    if len(distances) == 1:
        return distances[0]

    if take_max:
        return np.minimum(distances[0], distances[1])

    return weight * distances[0] + (1 - weight) * distances[1]


def count_common_oov_words(words1, words2, wd):
    """
    Return how many words are present in both `words1` and `words2`
    but are not in wd.
    """
    common_oov_words = [w for w in words1.intersection(words2)
                        if w not in wd]
    return len(common_oov_words)


def rerank(distances, faq_matrix, top_n=10, weight=0.05):
    """
    Rerank inplace the top distances for each questions.

    :param distances: matrix with shape (num_queries, num_faqs)
    :param faq_matrix: matrix with vector representations of the
        FAQs
    :param top_n: the top N candidates to rerank
    :param weight: the weight to give to the inter-faq distances
    """
    top_indices = distances.argsort(1)[:, :top_n]
    num_queries = distances.shape[0]

    # top_embedded_faqs is (num_queries, 10, embedding_size)
    top_embedded_faqs = faq_matrix[top_indices]

    # is there a better way to do this, without the loop?
    for i in range(num_queries):
        inter_faq_distances = cdist(top_embedded_faqs[i],
                                    top_embedded_faqs[i],
                                    'cosine')
        top_indices_this_query = top_indices[i]
        # distance_sum = inter_faq_distances.sum(1)

        # workaround to avoid the 0 (or close) similarity to itself
        cond = inter_faq_distances < 1e-10
        inter_faq_distances[cond] += 1000
        distance_min = inter_faq_distances.min(1)

        distances[i, top_indices_this_query] += distance_min * weight


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('queries', help='File with queries')
    parser.add_argument('FAQ', help='Input CSV file with the FAQ')
    parser.add_argument('embeddings', help='File with word embeddings '
                                           '(binary numpy)')
    parser.add_argument('dictionary', help='Word list corresponding to '
                                           'the embeddings')
    parser.add_argument('output', help='File to write the output')
    parser.add_argument('--df', help='JSON file with corpus to compute document'
                                     'frequency')
    parser.add_argument('-n', dest='top_n', type=int, default=10,
                        help='Number of words to include in SIDF')
    parser.add_argument('--mwe', dest='mwe',
                        help='File with MWEs (optional, one per line)')
    parser.add_argument('-c', dest='confidence', type=float,
                        help='Minimum confidence level', default=0)
    arggroup = parser.add_mutually_exclusive_group()
    arggroup.add_argument('-a', dest='use_answers', action='store_true',
                          help='Search similar answers instead of questions')
    arggroup.add_argument('-b', dest='use_both', action='store_true',
                          help='Use both answers and questions')
    parser.add_argument('-w', help='Weight to give to the answers '
                                   '(only makes sense with -b)',
                        dest='weight', default=0.5, type=float)
    parser.add_argument('--max', help='Choose the answer with most confidence '
                                      '(only makes sense with -b)',
                        action='store_true', dest='max')
    parser.add_argument('--acro', help='File with acronyms (each line with '
                        'acronym <TAB> full form)')
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    logging.info('Reading embeddings')
    np.random.seed(42)
    model = np.load(args.embeddings)
    wd = readdata.load_vocabulary(args.dictionary)

    mwes = readdata.read_mwes(args.mwe)
    acronyms = readdata.read_acronyms(args.acro)
    faq = readdata.read_faq(args.FAQ, acronyms)
    faq_ids = np.array([int(item[0]) for item in faq])

    texts = []
    if args.use_answers or args.use_both:
        answers = utils.get_tokenized_answers(faq, mwes)
        texts.append(answers)
        #TODO: this set is extracted again later on. avoid it.
        answer_word_set = get_word_set(answers)
        wd, model = create_new_embeddings(wd, model, answer_word_set)
    if not args.use_answers:
        questions = utils.get_tokenized_questions(faq, mwes)
        texts.append(questions)
        question_word_set = get_word_set(questions)
        wd, model = create_new_embeddings(wd, model, question_word_set)

    query_ids, queries = readdata.read_queries(args.queries, mwes, acronyms)
    tags = utils.get_tokenized_tags(faq, None)

    if args.mwe:
        mwes = readdata.read_mwes(args.mwe)
    else:
        mwes = set()

    query_words = get_word_set(queries)

    distance_matrices = []

    for questions_or_answers in texts:
        df = compute_df_from_questions_or_corpus(questions_or_answers, args.df, tags)

        if args.mwe:
            wd_mwe, model_mwe = generate_mwe_embeddings(df, wd, model)
        else:
            wd_mwe, model_mwe = wd, model

        faq_words = get_word_set(questions_or_answers)
        target_words = query_words.union(faq_words)
        sdf = compute_sdf(df, model_mwe, wd_mwe, target_words, args.top_n)

        # generate query_matrix inside the loop since it depends on the sdf
        faq_matrix = generate_embedding_matrix(questions_or_answers,
                                               wd_mwe, model_mwe, sdf)
        query_matrix = generate_embedding_matrix(queries, wd_mwe,
                                                 model_mwe, sdf)
        distances = compute_distances(query_matrix, faq_matrix)
        distance_matrices.append(distances)

    distances = combine_distances(distance_matrices, args.weight, args.max)

    print('')
    print('Writing results')
    print('===============\n')
    write_answers(args.output, distances, query_ids, faq_ids, args.confidence)
